#!/usr/bin/env python
# -*- coding: utf-8 -*-
# File: chanconfig/__init__.py
# Author: Jimin Huang <huangjimin@whu.edu.cn>
# Date: 19.10.2017
from config import Config, ReadConfigurationError


__all__ = ['Config', 'ReadConfigurationError']
__version__ = '0.0.4'
