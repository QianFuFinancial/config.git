#!/usr/bin/env python
# -*- coding: utf-8 -*-
# File: tests/test_config.py
# Author: Jimin Huang <huangjimin@whu.edu.cn>
# Date: 19.10.2017
import logging

from mock_logger import MockLoggingHandler
from nose.tools import assert_equals, assert_raises

from chanconfig import Config, ReadConfigurationError


HANDLER = MockLoggingHandler()


class TestConfig(object):
    """Test class for ``chanconfig.Config``
    """
    def setUp(self):
        HANDLER.reset()
        logging.getLogger('chanconfig.config').addHandler(HANDLER)
        self.expect_dict = {'test': 'test', 'test_nested': {'test': 'test'}}
        self.expect_repr = (
            "AttrDict({'test': 'test', 'test_nested': {'test': 'test'}})"
        )

    def test_with_package(self):
        """Check if ``chanconfig.Config`` works when given package and path
        """
        configs = Config("test.yml", "chanconfig.conf")

        # Check if __eq__ works
        assert_equals(configs, self.expect_dict)
        # Check if AttrDict get dict key as attribute
        assert_equals(configs.test, 'test')
        # Check if nested AttrDict get dict key as attribute
        assert_equals(configs.test_nested.test, 'test')
        # Check if AttrDict raises when key not found
        assert_raises(AttributeError, getattr, configs, 'non_exists')
        # Check if __repr__ works
        assert_equals(repr(configs), self.expect_repr)

        # Check info logs
        # Path changes with environments, unable to check detail
        assert HANDLER.messages['info']

    def test_without_package(self):
        """Check if ``chanconfig.Config`` works when only given path
        """
        configs = Config("chanconfig/conf/test.yml")

        # Check if __eq__ works
        assert_equals(configs, self.expect_dict)
        # Check if AttrDict get dict key as attribute
        assert_equals(configs.test, 'test')
        # Check if nested AttrDict get dict key as attribute
        assert_equals(configs.test_nested.test, 'test')
        # Check if AttrDict raises when key not found
        assert_raises(AttributeError, getattr, configs, 'non_exists')
        # Check if __repr__ works
        assert_equals(repr(configs), self.expect_repr)

        # Path changes with environments, unable to check detail
        assert HANDLER.messages['info']

    def test_package_not_exists(self):
        """Check if ``chanconfig.Config`` raises when package not exists
        """
        assert_raises(
            ReadConfigurationError, Config, "test.yml", "chanconfig.non_exists"
        )

        assert_equals(
            HANDLER.messages['error'], ['No module named non_exists']
        )
        # Check info logs with no logs
        assert_equals(HANDLER.messages['info'], [])

    def test_file_not_exists(self):
        """Check if ``chanconfig.Config`` raises when file not exists
        """
        assert_raises(
            ReadConfigurationError, Config, "test_non_exists.yml"
        )
        assert_equals(
            HANDLER.messages['error'],
            ["[Errno 2] No such file or directory: 'test_non_exists.yml'"]
        )
        # Check info logs with no logs
        assert_equals(HANDLER.messages['info'], [])

    def test_file_wrong_format(self):
        """Check if ``chanconfig.Config`` raises when file content wrong format
        """
        assert_raises(
            ReadConfigurationError, Config, "test_wrong.yml", "chanconfig.conf"
        )
        # Path changes with environments, unable to check detail
        assert HANDLER.messages['error']
        # Check info logs with no logs
        assert_equals(HANDLER.messages['info'], [])

    def test_update(self):
        """Check if ``chanconfig.Config.update`` works
        """
        configs = Config("test.yml", "chanconfig.conf")

        append_dict = {'test_append': 'test'}
        self.expect_dict.update(append_dict)

        configs.update(append_dict)

        # Check if __eq__ works
        assert_equals(configs, self.expect_dict)


def test_ReadConfigurationError():
    """Check if ``ReadConfigurationError`` works
    """
    error = ReadConfigurationError('test', 'test')
    assert_equals(str(error), 'Read test error: test')
    assert_equals(repr(error), 'ReadConfigurationError: test test')
